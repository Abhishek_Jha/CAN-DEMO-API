﻿namespace vCAN_Demo
{
    partial class Formmain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectdevice = new System.Windows.Forms.Button();
            this.DisconnectDevice = new System.Windows.Forms.Button();
            this.rr = new System.Windows.Forms.Button();
            this.normal = new System.Windows.Forms.RadioButton();
            this.listen_only = new System.Windows.Forms.RadioButton();
            this.loopback = new System.Windows.Forms.RadioButton();
            this.operating_mode = new System.Windows.Forms.GroupBox();
            this.menu = new System.Windows.Forms.GroupBox();
            this.fm_ver = new System.Windows.Forms.TextBox();
            this.hw_ver = new System.Windows.Forms.TextBox();
            this.getversion = new System.Windows.Forms.GroupBox();
            this.bd_rate_250 = new System.Windows.Forms.RadioButton();
            this.bd_rate_500 = new System.Windows.Forms.RadioButton();
            this.bd_rate_1000 = new System.Windows.Forms.RadioButton();
            this.baudrate = new System.Windows.Forms.GroupBox();
            this.Tx_Testmsg = new System.Windows.Forms.Button();
            this.RecieveMsg = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Monitor = new System.Windows.Forms.ListBox();
            this.TestMsg = new System.Windows.Forms.GroupBox();
            this.Tx_testMsg_29bit_remote_frame = new System.Windows.Forms.Button();
            this.Tx_testMsg_11bit_remote_frame = new System.Windows.Forms.Button();
            this.TX_testMsg_29bit_data_frame = new System.Windows.Forms.Button();
            this.none = new System.Windows.Forms.RadioButton();
            this.Data11BitOnly = new System.Windows.Forms.RadioButton();
            this.Data29BitOnly = new System.Windows.Forms.RadioButton();
            this.Filters = new System.Windows.Forms.GroupBox();
            this.custom11bitFilter = new System.Windows.Forms.RadioButton();
            this.customFilter29bit = new System.Windows.Forms.RadioButton();
            this.clearBuffer = new System.Windows.Forms.Button();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.DeviceBehaviour = new System.Windows.Forms.GroupBox();
            this.getdllver = new System.Windows.Forms.Button();
            this.operating_mode.SuspendLayout();
            this.menu.SuspendLayout();
            this.getversion.SuspendLayout();
            this.baudrate.SuspendLayout();
            this.TestMsg.SuspendLayout();
            this.Filters.SuspendLayout();
            this.DeviceBehaviour.SuspendLayout();
            this.SuspendLayout();
            // 
            // connectdevice
            // 
            this.connectdevice.Location = new System.Drawing.Point(19, 19);
            this.connectdevice.Name = "connectdevice";
            this.connectdevice.Size = new System.Drawing.Size(75, 23);
            this.connectdevice.TabIndex = 0;
            this.connectdevice.Text = "connect";
            this.connectdevice.UseVisualStyleBackColor = true;
            this.connectdevice.Click += new System.EventHandler(this.connectdevice_Click);
            // 
            // DisconnectDevice
            // 
            this.DisconnectDevice.Enabled = false;
            this.DisconnectDevice.Location = new System.Drawing.Point(127, 19);
            this.DisconnectDevice.Name = "DisconnectDevice";
            this.DisconnectDevice.Size = new System.Drawing.Size(75, 23);
            this.DisconnectDevice.TabIndex = 1;
            this.DisconnectDevice.Text = "Disconnect";
            this.DisconnectDevice.UseVisualStyleBackColor = true;
            this.DisconnectDevice.Click += new System.EventHandler(this.DisconnectDevice_Click);
            // 
            // rr
            // 
            this.rr.Location = new System.Drawing.Point(230, 19);
            this.rr.Name = "rr";
            this.rr.Size = new System.Drawing.Size(75, 23);
            this.rr.TabIndex = 2;
            this.rr.Text = "Version";
            this.rr.UseVisualStyleBackColor = true;
            this.rr.Click += new System.EventHandler(this.rr_Click);
            // 
            // normal
            // 
            this.normal.AutoSize = true;
            this.normal.Location = new System.Drawing.Point(9, 19);
            this.normal.Name = "normal";
            this.normal.Size = new System.Drawing.Size(56, 17);
            this.normal.TabIndex = 3;
            this.normal.Text = "normal";
            this.normal.UseVisualStyleBackColor = true;
            this.normal.CheckedChanged += new System.EventHandler(this.normal_CheckedChanged);
            // 
            // listen_only
            // 
            this.listen_only.AutoSize = true;
            this.listen_only.Checked = true;
            this.listen_only.Location = new System.Drawing.Point(81, 19);
            this.listen_only.Name = "listen_only";
            this.listen_only.Size = new System.Drawing.Size(71, 17);
            this.listen_only.TabIndex = 4;
            this.listen_only.TabStop = true;
            this.listen_only.Text = "listen only";
            this.listen_only.UseVisualStyleBackColor = true;
            this.listen_only.CheckedChanged += new System.EventHandler(this.listen_only_CheckedChanged);
            // 
            // loopback
            // 
            this.loopback.AutoSize = true;
            this.loopback.Location = new System.Drawing.Point(169, 19);
            this.loopback.Name = "loopback";
            this.loopback.Size = new System.Drawing.Size(69, 17);
            this.loopback.TabIndex = 5;
            this.loopback.Text = "loopback";
            this.loopback.UseVisualStyleBackColor = true;
            this.loopback.CheckedChanged += new System.EventHandler(this.loopback_CheckedChanged);
            // 
            // operating_mode
            // 
            this.operating_mode.Controls.Add(this.normal);
            this.operating_mode.Controls.Add(this.loopback);
            this.operating_mode.Controls.Add(this.listen_only);
            this.operating_mode.Enabled = false;
            this.operating_mode.Location = new System.Drawing.Point(12, 74);
            this.operating_mode.Name = "operating_mode";
            this.operating_mode.Size = new System.Drawing.Size(315, 53);
            this.operating_mode.TabIndex = 6;
            this.operating_mode.TabStop = false;
            this.operating_mode.Text = "operating mode";
            // 
            // menu
            // 
            this.menu.Controls.Add(this.connectdevice);
            this.menu.Controls.Add(this.DisconnectDevice);
            this.menu.Location = new System.Drawing.Point(21, 12);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(306, 56);
            this.menu.TabIndex = 7;
            this.menu.TabStop = false;
            this.menu.Text = "menu";
            // 
            // fm_ver
            // 
            this.fm_ver.Location = new System.Drawing.Point(9, 21);
            this.fm_ver.Name = "fm_ver";
            this.fm_ver.Size = new System.Drawing.Size(100, 20);
            this.fm_ver.TabIndex = 8;
            this.fm_ver.TextChanged += new System.EventHandler(this.fm_ver_TextChanged);
            // 
            // hw_ver
            // 
            this.hw_ver.Location = new System.Drawing.Point(115, 22);
            this.hw_ver.Name = "hw_ver";
            this.hw_ver.Size = new System.Drawing.Size(100, 20);
            this.hw_ver.TabIndex = 9;
            this.hw_ver.TextChanged += new System.EventHandler(this.hw_ver_TextChanged);
            // 
            // getversion
            // 
            this.getversion.Controls.Add(this.rr);
            this.getversion.Controls.Add(this.fm_ver);
            this.getversion.Controls.Add(this.hw_ver);
            this.getversion.Enabled = false;
            this.getversion.Location = new System.Drawing.Point(12, 133);
            this.getversion.Name = "getversion";
            this.getversion.Size = new System.Drawing.Size(315, 57);
            this.getversion.TabIndex = 10;
            this.getversion.TabStop = false;
            this.getversion.Text = "Get_Version";
            // 
            // bd_rate_250
            // 
            this.bd_rate_250.AutoSize = true;
            this.bd_rate_250.Location = new System.Drawing.Point(9, 19);
            this.bd_rate_250.Name = "bd_rate_250";
            this.bd_rate_250.Size = new System.Drawing.Size(69, 17);
            this.bd_rate_250.TabIndex = 11;
            this.bd_rate_250.Text = "250 kbps";
            this.bd_rate_250.UseVisualStyleBackColor = true;
            this.bd_rate_250.CheckedChanged += new System.EventHandler(this.bd_rate_250_CheckedChanged);
            // 
            // bd_rate_500
            // 
            this.bd_rate_500.AutoSize = true;
            this.bd_rate_500.Location = new System.Drawing.Point(97, 19);
            this.bd_rate_500.Name = "bd_rate_500";
            this.bd_rate_500.Size = new System.Drawing.Size(66, 17);
            this.bd_rate_500.TabIndex = 12;
            this.bd_rate_500.Text = "500kbps";
            this.bd_rate_500.UseVisualStyleBackColor = true;
            this.bd_rate_500.CheckedChanged += new System.EventHandler(this.bd_rate_500_CheckedChanged);
            // 
            // bd_rate_1000
            // 
            this.bd_rate_1000.AutoSize = true;
            this.bd_rate_1000.Checked = true;
            this.bd_rate_1000.Location = new System.Drawing.Point(184, 19);
            this.bd_rate_1000.Name = "bd_rate_1000";
            this.bd_rate_1000.Size = new System.Drawing.Size(72, 17);
            this.bd_rate_1000.TabIndex = 13;
            this.bd_rate_1000.TabStop = true;
            this.bd_rate_1000.Text = "1000kbps";
            this.bd_rate_1000.UseVisualStyleBackColor = true;
            this.bd_rate_1000.CheckedChanged += new System.EventHandler(this.bd_rate_1000_CheckedChanged);
            // 
            // baudrate
            // 
            this.baudrate.Controls.Add(this.bd_rate_250);
            this.baudrate.Controls.Add(this.bd_rate_1000);
            this.baudrate.Controls.Add(this.bd_rate_500);
            this.baudrate.Enabled = false;
            this.baudrate.Location = new System.Drawing.Point(12, 196);
            this.baudrate.Name = "baudrate";
            this.baudrate.Size = new System.Drawing.Size(315, 57);
            this.baudrate.TabIndex = 14;
            this.baudrate.TabStop = false;
            this.baudrate.Text = "baudrate";
            this.baudrate.Enter += new System.EventHandler(this.baudrate_Enter);
            // 
            // Tx_Testmsg
            // 
            this.Tx_Testmsg.Enabled = false;
            this.Tx_Testmsg.Location = new System.Drawing.Point(21, 19);
            this.Tx_Testmsg.Name = "Tx_Testmsg";
            this.Tx_Testmsg.Size = new System.Drawing.Size(82, 23);
            this.Tx_Testmsg.TabIndex = 15;
            this.Tx_Testmsg.Text = "Tx_Testmsg(11bit data frame)";
            this.Tx_Testmsg.UseVisualStyleBackColor = true;
            this.Tx_Testmsg.Click += new System.EventHandler(this.Tx_Testmsg_Click);
            // 
            // RecieveMsg
            // 
            this.RecieveMsg.Enabled = false;
            this.RecieveMsg.Location = new System.Drawing.Point(344, 431);
            this.RecieveMsg.Name = "RecieveMsg";
            this.RecieveMsg.Size = new System.Drawing.Size(75, 23);
            this.RecieveMsg.TabIndex = 16;
            this.RecieveMsg.Text = "Show Msg";
            this.RecieveMsg.UseVisualStyleBackColor = true;
            this.RecieveMsg.Click += new System.EventHandler(this.RecieveMsg_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(333, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Type\r\n";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(365, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "BitID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(401, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(425, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 21;
            this.label4.Text = "Frame";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(479, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "DLC";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(513, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "D1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(531, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "D2";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(558, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 25;
            this.label8.Text = "D3";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(585, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "D4";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(603, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "D5";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(630, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "D6";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(657, 15);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "D7";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(684, 15);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(21, 13);
            this.label13.TabIndex = 30;
            this.label13.Text = "D8";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(733, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "Timer";
            // 
            // Monitor
            // 
            this.Monitor.FormattingEnabled = true;
            this.Monitor.Location = new System.Drawing.Point(336, 34);
            this.Monitor.Name = "Monitor";
            this.Monitor.Size = new System.Drawing.Size(574, 394);
            this.Monitor.TabIndex = 33;
            // 
            // TestMsg
            // 
            this.TestMsg.Controls.Add(this.Tx_testMsg_29bit_remote_frame);
            this.TestMsg.Controls.Add(this.Tx_testMsg_11bit_remote_frame);
            this.TestMsg.Controls.Add(this.TX_testMsg_29bit_data_frame);
            this.TestMsg.Controls.Add(this.Tx_Testmsg);
            this.TestMsg.Enabled = false;
            this.TestMsg.Location = new System.Drawing.Point(12, 259);
            this.TestMsg.Name = "TestMsg";
            this.TestMsg.Size = new System.Drawing.Size(315, 73);
            this.TestMsg.TabIndex = 34;
            this.TestMsg.TabStop = false;
            this.TestMsg.Text = "TestMsg";
            this.TestMsg.Enter += new System.EventHandler(this.TestMsg_Enter);
            // 
            // Tx_testMsg_29bit_remote_frame
            // 
            this.Tx_testMsg_29bit_remote_frame.Location = new System.Drawing.Point(110, 48);
            this.Tx_testMsg_29bit_remote_frame.Name = "Tx_testMsg_29bit_remote_frame";
            this.Tx_testMsg_29bit_remote_frame.Size = new System.Drawing.Size(83, 22);
            this.Tx_testMsg_29bit_remote_frame.TabIndex = 37;
            this.Tx_testMsg_29bit_remote_frame.Text = "Tx_testMsg_29bit_remote_frame";
            this.Tx_testMsg_29bit_remote_frame.UseVisualStyleBackColor = true;
            this.Tx_testMsg_29bit_remote_frame.Click += new System.EventHandler(this.Tx_testMsg_29bit_remote_frame_Click);
            // 
            // Tx_testMsg_11bit_remote_frame
            // 
            this.Tx_testMsg_11bit_remote_frame.Location = new System.Drawing.Point(109, 19);
            this.Tx_testMsg_11bit_remote_frame.Name = "Tx_testMsg_11bit_remote_frame";
            this.Tx_testMsg_11bit_remote_frame.Size = new System.Drawing.Size(84, 23);
            this.Tx_testMsg_11bit_remote_frame.TabIndex = 36;
            this.Tx_testMsg_11bit_remote_frame.Text = "Tx_testMsg_11bit_remote_frame";
            this.Tx_testMsg_11bit_remote_frame.UseVisualStyleBackColor = true;
            this.Tx_testMsg_11bit_remote_frame.Click += new System.EventHandler(this.Tx_testMsg_11bit_remote_frame_Click);
            // 
            // TX_testMsg_29bit_data_frame
            // 
            this.TX_testMsg_29bit_data_frame.Location = new System.Drawing.Point(21, 48);
            this.TX_testMsg_29bit_data_frame.Name = "TX_testMsg_29bit_data_frame";
            this.TX_testMsg_29bit_data_frame.Size = new System.Drawing.Size(82, 22);
            this.TX_testMsg_29bit_data_frame.TabIndex = 35;
            this.TX_testMsg_29bit_data_frame.Text = "TX_testMsg(29bit data frame)";
            this.TX_testMsg_29bit_data_frame.UseVisualStyleBackColor = true;
            this.TX_testMsg_29bit_data_frame.Click += new System.EventHandler(this.TX_testMsg_29bit_data_frame_Click);
            // 
            // none
            // 
            this.none.AutoSize = true;
            this.none.Checked = true;
            this.none.Location = new System.Drawing.Point(9, 19);
            this.none.Name = "none";
            this.none.Size = new System.Drawing.Size(51, 17);
            this.none.TabIndex = 35;
            this.none.TabStop = true;
            this.none.Text = "None";
            this.none.UseVisualStyleBackColor = true;
            this.none.CheckedChanged += new System.EventHandler(this.none_CheckedChanged);
            // 
            // Data11BitOnly
            // 
            this.Data11BitOnly.AutoSize = true;
            this.Data11BitOnly.Location = new System.Drawing.Point(81, 19);
            this.Data11BitOnly.Name = "Data11BitOnly";
            this.Data11BitOnly.Size = new System.Drawing.Size(102, 17);
            this.Data11BitOnly.TabIndex = 36;
            this.Data11BitOnly.Text = "11 Bit Data Only";
            this.Data11BitOnly.UseVisualStyleBackColor = true;
            this.Data11BitOnly.CheckedChanged += new System.EventHandler(this.Data11BitOnly_CheckedChanged);
            // 
            // Data29BitOnly
            // 
            this.Data29BitOnly.AutoSize = true;
            this.Data29BitOnly.Location = new System.Drawing.Point(203, 19);
            this.Data29BitOnly.Name = "Data29BitOnly";
            this.Data29BitOnly.Size = new System.Drawing.Size(102, 17);
            this.Data29BitOnly.TabIndex = 37;
            this.Data29BitOnly.Text = "29 Bit Data Only";
            this.Data29BitOnly.UseVisualStyleBackColor = true;
            this.Data29BitOnly.CheckedChanged += new System.EventHandler(this.Data29BitOnly_CheckedChanged);
            // 
            // Filters
            // 
            this.Filters.Controls.Add(this.custom11bitFilter);
            this.Filters.Controls.Add(this.none);
            this.Filters.Controls.Add(this.customFilter29bit);
            this.Filters.Controls.Add(this.Data29BitOnly);
            this.Filters.Controls.Add(this.Data11BitOnly);
            this.Filters.Enabled = false;
            this.Filters.Location = new System.Drawing.Point(12, 378);
            this.Filters.Name = "Filters";
            this.Filters.Size = new System.Drawing.Size(315, 67);
            this.Filters.TabIndex = 38;
            this.Filters.TabStop = false;
            this.Filters.Text = "Fixed Filters";
            // 
            // custom11bitFilter
            // 
            this.custom11bitFilter.AutoSize = true;
            this.custom11bitFilter.Location = new System.Drawing.Point(28, 42);
            this.custom11bitFilter.Name = "custom11bitFilter";
            this.custom11bitFilter.Size = new System.Drawing.Size(86, 17);
            this.custom11bitFilter.TabIndex = 0;
            this.custom11bitFilter.TabStop = true;
            this.custom11bitFilter.Text = "Custom 11bit";
            this.custom11bitFilter.UseVisualStyleBackColor = true;
            this.custom11bitFilter.CheckedChanged += new System.EventHandler(this.custom11bitFilter_CheckedChanged);
            // 
            // customFilter29bit
            // 
            this.customFilter29bit.AutoSize = true;
            this.customFilter29bit.Location = new System.Drawing.Point(136, 42);
            this.customFilter29bit.Name = "customFilter29bit";
            this.customFilter29bit.Size = new System.Drawing.Size(86, 17);
            this.customFilter29bit.TabIndex = 1;
            this.customFilter29bit.TabStop = true;
            this.customFilter29bit.Text = "Custom 29bit";
            this.customFilter29bit.UseVisualStyleBackColor = true;
            this.customFilter29bit.CheckedChanged += new System.EventHandler(this.customFilter29bit_CheckedChanged);
            // 
            // clearBuffer
            // 
            this.clearBuffer.Enabled = false;
            this.clearBuffer.Location = new System.Drawing.Point(446, 431);
            this.clearBuffer.Name = "clearBuffer";
            this.clearBuffer.Size = new System.Drawing.Size(75, 23);
            this.clearBuffer.TabIndex = 39;
            this.clearBuffer.Text = "clearBuffer";
            this.clearBuffer.UseVisualStyleBackColor = true;
            this.clearBuffer.Click += new System.EventHandler(this.button1_Click);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(81, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(79, 17);
            this.radioButton1.TabIndex = 38;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "RxMsgOnly";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(184, 19);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(78, 17);
            this.radioButton2.TabIndex = 39;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "TxMsgOnly";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(18, 19);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(47, 17);
            this.radioButton3.TabIndex = 40;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Both";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // DeviceBehaviour
            // 
            this.DeviceBehaviour.Controls.Add(this.radioButton3);
            this.DeviceBehaviour.Controls.Add(this.radioButton2);
            this.DeviceBehaviour.Controls.Add(this.radioButton1);
            this.DeviceBehaviour.Enabled = false;
            this.DeviceBehaviour.Location = new System.Drawing.Point(12, 335);
            this.DeviceBehaviour.Name = "DeviceBehaviour";
            this.DeviceBehaviour.Size = new System.Drawing.Size(315, 46);
            this.DeviceBehaviour.TabIndex = 41;
            this.DeviceBehaviour.TabStop = false;
            this.DeviceBehaviour.Text = "DeviceBehaviour";
            // 
            // getdllver
            // 
            this.getdllver.Enabled = false;
            this.getdllver.Location = new System.Drawing.Point(549, 431);
            this.getdllver.Name = "getdllver";
            this.getdllver.Size = new System.Drawing.Size(75, 23);
            this.getdllver.TabIndex = 42;
            this.getdllver.Text = "Get Dll Ver.";
            this.getdllver.UseVisualStyleBackColor = true;
            this.getdllver.Click += new System.EventHandler(this.button2_Click);
            // 
            // Formmain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 457);
            this.Controls.Add(this.getdllver);
            this.Controls.Add(this.DeviceBehaviour);
            this.Controls.Add(this.clearBuffer);
            this.Controls.Add(this.Filters);
            this.Controls.Add(this.TestMsg);
            this.Controls.Add(this.RecieveMsg);
            this.Controls.Add(this.Monitor);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.baudrate);
            this.Controls.Add(this.getversion);
            this.Controls.Add(this.menu);
            this.Controls.Add(this.operating_mode);
            this.Name = "Formmain";
            this.Text = "vCAN C# Demo API";
            this.operating_mode.ResumeLayout(false);
            this.operating_mode.PerformLayout();
            this.menu.ResumeLayout(false);
            this.getversion.ResumeLayout(false);
            this.getversion.PerformLayout();
            this.baudrate.ResumeLayout(false);
            this.baudrate.PerformLayout();
            this.TestMsg.ResumeLayout(false);
            this.Filters.ResumeLayout(false);
            this.Filters.PerformLayout();
            this.DeviceBehaviour.ResumeLayout(false);
            this.DeviceBehaviour.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connectdevice;
        private System.Windows.Forms.Button DisconnectDevice;
        private System.Windows.Forms.Button rr;
        private System.Windows.Forms.RadioButton normal;
        private System.Windows.Forms.RadioButton listen_only;
        private System.Windows.Forms.RadioButton loopback;
        private System.Windows.Forms.GroupBox operating_mode;
        private System.Windows.Forms.GroupBox menu;
        private System.Windows.Forms.TextBox fm_ver;
        private System.Windows.Forms.TextBox hw_ver;
        private System.Windows.Forms.GroupBox getversion;
        private System.Windows.Forms.RadioButton bd_rate_250;
        private System.Windows.Forms.RadioButton bd_rate_500;
        private System.Windows.Forms.RadioButton bd_rate_1000;
        private System.Windows.Forms.GroupBox baudrate;
        private System.Windows.Forms.Button Tx_Testmsg;
        private System.Windows.Forms.Button RecieveMsg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ListBox Monitor;
        private System.Windows.Forms.GroupBox TestMsg;
        private System.Windows.Forms.Button Tx_testMsg_29bit_remote_frame;
        private System.Windows.Forms.Button Tx_testMsg_11bit_remote_frame;
        private System.Windows.Forms.Button TX_testMsg_29bit_data_frame;
        private System.Windows.Forms.RadioButton none;
        private System.Windows.Forms.RadioButton Data11BitOnly;
        private System.Windows.Forms.RadioButton Data29BitOnly;
        private System.Windows.Forms.GroupBox Filters;
        private System.Windows.Forms.RadioButton custom11bitFilter;
        private System.Windows.Forms.RadioButton customFilter29bit;
        private System.Windows.Forms.Button clearBuffer;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.GroupBox DeviceBehaviour;
        private System.Windows.Forms.Button getdllver;
    }
}

