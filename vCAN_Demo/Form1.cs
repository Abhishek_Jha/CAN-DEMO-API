﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CAN_BUS_API;


namespace vCAN_Demo
{
    public partial class Formmain : Form
    {
        vCAN_API vCANdemo = new vCAN_API();
        public Formmain()
        {
            // vCANdemo.InitializeDeviceParameter();
            InitializeComponent();
        }

        private void rr_Click(object sender, EventArgs e)
        {
            string sFirmwareversion;
            string sHWversion;
            sFirmwareversion = vCANdemo.vCAN_getFirmWareVersion();
            sHWversion = vCANdemo.vCAN_getHardWareVersion();
            //convert Byte to Hexa and display as decimal : 0X01 to .1
            hw_ver.Text = "H/W:" + sHWversion;
            fm_ver.Text = "F/W:" + sFirmwareversion;
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {
            vCANdemo.vCAN_DisconnectDevice();
        }

        private void connectdevice_Click(object sender, EventArgs e)
        {
            if (vCANdemo.vCAN_ConnectDevice())
            {
                
                vCANdemo.InitializeReader();
                vCANdemo.InitializeWriter();
                connectdevice.Enabled = false;
                DisconnectDevice.Enabled = true;
                Tx_Testmsg.Enabled = true;
                RecieveMsg.Enabled = true;
                operating_mode.Enabled = true;
                getversion.Enabled = true;
                baudrate.Enabled = true;
                TestMsg.Enabled = true;
                Filters.Enabled = true;
                DeviceBehaviour.Enabled = true;
                getdllver.Enabled = true;
                clearBuffer.Enabled = true;
            }
            else
            {
                MessageBox.Show("Device not connected");
                return;
            }
        }


        private void DisconnectDevice_Click(object sender, EventArgs e)
        {
            connectdevice.Enabled = true;
            DisconnectDevice.Enabled = false;
            Tx_Testmsg.Enabled = false;
            RecieveMsg.Enabled = false;
            operating_mode.Enabled = false;
            getversion.Enabled = false;
            baudrate.Enabled = false;
            TestMsg.Enabled = false;
            Filters.Enabled = false;
            DeviceBehaviour.Enabled = false;
            getdllver.Enabled = false;
            clearBuffer.Enabled = false;
            // DisableOnTime500Timer();

            // lbl_Status.Text = "Disconnected";
            //this.lbl_Status.ForeColor = System.Drawing.Color.Red;

            if (vCANdemo != null)
                vCANdemo.vCAN_DisconnectDevice();
        }

        private void loopback_CheckedChanged(object sender, EventArgs e)
        {
            if (loopback.Checked)
            {
                vCANdemo.vCAN_Set_operatingMode_loopback();

            }
        }

        private void listen_only_CheckedChanged(object sender, EventArgs e)
        {
            if (listen_only.Checked)
            {
                vCANdemo.vCAN_Set_operatingMode_listen_only();

            }
        }

        private void normal_CheckedChanged(object sender, EventArgs e)
        {
            if (normal.Checked)
            {
                vCANdemo.vCAN_Set_operatingMode_normal();

            }
        }

        private void bd_rate_250_CheckedChanged(object sender, EventArgs e)
        {
            if (bd_rate_250.Checked)
            {

                vCANdemo.vcan_SetBaudrate_250kbps();
            }
        }

        private void bd_rate_500_CheckedChanged(object sender, EventArgs e)
        {
            if (bd_rate_500.Checked)
            {

                vCANdemo.vcan_SetBaudrate_500kbps();
            }
        }

        private void bd_rate_1000_CheckedChanged(object sender, EventArgs e)
        {
            if (bd_rate_1000.Checked)
            {

                vCANdemo.vcan_SetBaudrate_1000kbps();
            }
        }

        private void Tx_Testmsg_Click(object sender, EventArgs e)
        {
            uint MessageID = 2047;  //maximum 11bit message(0x7ff)
            byte DLC = Convert.ToByte(2);
            byte messageno = Convert.ToByte(1);
            byte[] transmitBuffer1 = new byte[8]{ 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(1);
            transmitBuffer1[1] = Convert.ToByte(2);
            transmitBuffer1[2] = Convert.ToByte(3);
            transmitBuffer1[3] = Convert.ToByte(4);
            transmitBuffer1[4] = Convert.ToByte(5);
            transmitBuffer1[5] = Convert.ToByte(6);
            transmitBuffer1[6] = Convert.ToByte(7);
            transmitBuffer1[7] = Convert.ToByte(8);

            vCANdemo.vCAN_Transmit_Msg_11bit_data_frame(messageno, MessageID, DLC, transmitBuffer1);
        }

        private void RecieveMsg_Click(object sender, EventArgs e)
        {
            byte[] ReceivedMsg = new byte[20] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ReceivedMsg = vCANdemo.vCAN_Receive_Msg();
            Monitor.Items.Add(ReceivedMsg[0] + "       " + "          " + ReceivedMsg[1] + " " + ReceivedMsg[2] + " " + ReceivedMsg[3] + " " + ReceivedMsg[4] +"   " +ReceivedMsg[5] + "             " + "        " + ReceivedMsg[6] + "        " + ReceivedMsg[7] + "     " + ReceivedMsg[8] + "      " + ReceivedMsg[9] + "      " + ReceivedMsg[10] + "      " + ReceivedMsg[11] + "      " + ReceivedMsg[12] + "      " + ReceivedMsg[13] + "      " + ReceivedMsg[14]);
            //Monitor.Items.Add(ReceivedMsg[1] + "" + ReceivedMsg[2] + "" + ReceivedMsg[3] + "" + ReceivedMsg[4] + "" + ReceivedMsg[6] + "" + ReceivedMsg[7] + "" + ReceivedMsg[8] + "" + ReceivedMsg[9] + "" + ReceivedMsg[10] + "" + ReceivedMsg[11] + "" + ReceivedMsg[12] + "" + ReceivedMsg[13] + "" + ReceivedMsg[14]);
            
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void TX_testMsg_29bit_data_frame_Click(object sender, EventArgs e)
        {
            uint MessageID = 2097151;//Max 29 bit message (0x1fffff)
            byte DLC = Convert.ToByte(4);
            byte messageno = Convert.ToByte(1);
            byte[] transmitBuffer1 = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(1);
            transmitBuffer1[1] = Convert.ToByte(2);
            transmitBuffer1[2] = Convert.ToByte(3);
            transmitBuffer1[3] = Convert.ToByte(4);
            transmitBuffer1[4] = Convert.ToByte(5);
            transmitBuffer1[5] = Convert.ToByte(6);
            transmitBuffer1[6] = Convert.ToByte(7);
            transmitBuffer1[7] = Convert.ToByte(8);
            vCANdemo.vCAN_Transmit_Msg_29bit_data_frame(messageno, MessageID, DLC, transmitBuffer1);
        }

        private void Tx_testMsg_11bit_remote_frame_Click(object sender, EventArgs e)
        {
            uint MessageID = 33;
            byte DLC = Convert.ToByte(6);
            byte messageno = Convert.ToByte(1);
            byte[] transmitBuffer1 = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(1);
            transmitBuffer1[1] = Convert.ToByte(2);
            transmitBuffer1[2] = Convert.ToByte(3);
            transmitBuffer1[3] = Convert.ToByte(4);
            transmitBuffer1[4] = Convert.ToByte(5);
            transmitBuffer1[5] = Convert.ToByte(6);
            transmitBuffer1[6] = Convert.ToByte(7);
            transmitBuffer1[7] = Convert.ToByte(8);
            vCANdemo.vCAN_Transmit_Msg_11bit_data_frame(messageno, MessageID, DLC, transmitBuffer1);
        }

        private void Tx_testMsg_29bit_remote_frame_Click(object sender, EventArgs e)
        {
            uint MessageID = 43;
            byte DLC = Convert.ToByte(8);
            byte messageno = Convert.ToByte(1);
            byte[] transmitBuffer1 = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(1);
            transmitBuffer1[1] = Convert.ToByte(2);
            transmitBuffer1[2] = Convert.ToByte(3);
            transmitBuffer1[3] = Convert.ToByte(4);
            transmitBuffer1[4] = Convert.ToByte(5);
            transmitBuffer1[5] = Convert.ToByte(6);
            transmitBuffer1[6] = Convert.ToByte(7);
            transmitBuffer1[7] = Convert.ToByte(8);
            vCANdemo.vCAN_Transmit_Msg_29bit_data_frame(messageno, MessageID, DLC, transmitBuffer1);
        }

        private void none_CheckedChanged(object sender, EventArgs e)
        {
            vCANdemo.vCANSetFilterNone();
        }

        private void Data11BitOnly_CheckedChanged(object sender, EventArgs e)
        {
            vCANdemo.vCANSetFilterAll_11bit();
        }

        private void Data29BitOnly_CheckedChanged(object sender, EventArgs e)
        {
            vCANdemo.vCANSetFilterAll_29bit();
        }

        private void custom11bitFilter_CheckedChanged(object sender, EventArgs e)
        {
            uint MaskId = Convert.ToByte(29);
            uint FilterA = Convert.ToByte(8);
            uint FilterB = Convert.ToByte(1);
            uint FIlterC = Convert.ToByte(00);
            byte[] transmitBuffer1 = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(1);
            transmitBuffer1[1] = Convert.ToByte(2);
            transmitBuffer1[2] = Convert.ToByte(3);
            transmitBuffer1[3] = Convert.ToByte(4);
            transmitBuffer1[4] = Convert.ToByte(5);
            transmitBuffer1[5] = Convert.ToByte(6);
            transmitBuffer1[6] = Convert.ToByte(7);
            transmitBuffer1[7] = Convert.ToByte(8);
            vCANdemo.SetCustomFilter_11bit(MaskId, FilterA, FilterB, FIlterC);
        }  

        private void customFilter29bit_CheckedChanged(object sender, EventArgs e)
        {
            uint MaskId = Convert.ToByte(29);
            uint FilterA = Convert.ToByte(8);
            uint FilterB = Convert.ToByte(1);
            uint FIlterC = Convert.ToByte(00);
            byte[] transmitBuffer1 = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
            transmitBuffer1[0] = Convert.ToByte(1);
            transmitBuffer1[1] = Convert.ToByte(2);
            transmitBuffer1[2] = Convert.ToByte(3);
            transmitBuffer1[3] = Convert.ToByte(4);
            transmitBuffer1[4] = Convert.ToByte(5);
            transmitBuffer1[5] = Convert.ToByte(6);
            transmitBuffer1[6] = Convert.ToByte(7);
            transmitBuffer1[7] = Convert.ToByte(8);
            vCANdemo.SetCustomFilter_29bit(MaskId, FilterA, FilterB, FIlterC);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            vCANdemo.vCANClearDevicBuffer();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            vCANdemo.vCANSetRxMsgOnly();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            vCANdemo.vCANSetBufferBehaviourBoth();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            vCANdemo.vCANSetTxMsgOnly();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            vCANdemo.vCANDLLVersion();
            MessageBox.Show("The Dll ver is :" + vCANdemo.DllVer);
        }

        private void TestMsg_Enter(object sender, EventArgs e)
        {

        }

        private void baudrate_Enter(object sender, EventArgs e)
        {

        }

        private void hw_ver_TextChanged(object sender, EventArgs e)
        {

        }

        private void fm_ver_TextChanged(object sender, EventArgs e)
        {

        }

        private void dllver_TextChanged(object sender, EventArgs e)
        {
            vCANdemo.vCANDLLVersion();
        }

        

    }
}

